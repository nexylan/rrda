FROM golang:1-alpine3.11 as build
RUN apk add --no-cache git~=2
RUN git clone --branch 1.1.0 https://github.com/fcambus/rrda.git /build
WORKDIR /build
RUN CGO_ENABLED=0 GOOS=linux go build

FROM scratch as app
EXPOSE 80
COPY --from=build /build/rrda /rrda
CMD ["/rrda", "-host", "0.0.0.0", "-port", "80"]
