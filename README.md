# PDF

REST API allowing to perform DNS queries over HTTP.

https://rrda.nexylan.dev

Original project: https://github.com/fcambus/rrda

## Usage

```
curl https://rrda.nexylan.dev/8.8.8.8:53/nexylan.com/A
{
    "question": [
        {
            "name": "nexylan.com.",
            "type": "A",
            "class": "IN"
        }
    ],
    "answer": [
        {
            "name": "nexylan.com.",
            "type": "A",
            "class": "IN",
            "ttl": 599,
            "rdlength": 4,
            "rdata": "104.31.84.164"
        },
        {
            "name": "nexylan.com.",
            "type": "A",
            "class": "IN",
            "ttl": 599,
            "rdlength": 4,
            "rdata": "104.31.85.164"
        }
    ]
}
```

More information about how to make queries: https://github.com/fcambus/rrda#making-queries
